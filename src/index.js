import React from 'react';
import ReactDOM from 'react-dom';
import App from './App'; // Importez votre composant App depuis le fichier approprié
import { BrowserRouter } from 'react-router-dom';


ReactDOM.render(
  <BrowserRouter>
    <App /> {/* Rendu du composant App dans l'élément root de l'application */}
  </BrowserRouter>,
  document.getElementById('root')
);
