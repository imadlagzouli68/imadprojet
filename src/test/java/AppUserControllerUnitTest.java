import com.example.demosecurity3.config.controller.AppUserController;
import com.example.demosecurity3.config.entities.AppRole;
import com.example.demosecurity3.config.entities.AppUser;
import com.example.demosecurity3.config.service.AccountService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AppUserControllerUnitTest {

    // Mock du service AccountService pour simuler les comportements du service
    @Mock
    private AccountService accountService;

    // Injecte le mock du service dans le contrôleur à tester
    @InjectMocks
    private AppUserController appUserController;
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAppUsers() {
        // Créez une liste fictive d'utilisateurs pour simuler la réponse du service
        List<AppUser> users = Arrays.asList(
                new AppUser(1L, "user1", "password1", "user1@example.com", null),
                new AppUser(2L, "user2", "password2", "user2@example.com", null)
        );

        // Définir le comportement du mock du service lorsqu'il est appelé
        when(accountService.listUsers()).thenReturn(users);

        // Appeler la méthode du contrôleur à tester
        List<AppUser> result = appUserController.appUsers();

        // Vérifier si la méthode du service a été appelée une fois
        verify(accountService, times(1)).listUsers();

        // Vérifier si la liste retournée par le contrôleur correspond à celle retournée par le service
        assertEquals(users, result);
    }
    @Test
    public void testProfile() {
        // Créer un utilisateur fictif
        AppUser user = new AppUser(1L, "user1", "password1", "user1@example.com", Collections.emptyList());

        // Créer un Principal fictif
        Principal principal = () -> user.getUsername();

        // Définir le comportement du mock du service lorsqu'il est appelé
        when(accountService.loadUserByUsername(user.getUsername())).thenReturn(user);

        // Appeler la méthode du contrôleur à tester
        AppUser result = appUserController.profile(principal);

        // Vérifier si la méthode du service a été appelée une fois
        verify(accountService, times(1)).loadUserByUsername(user.getUsername());

        // Vérifier si l'utilisateur retourné par le contrôleur correspond à celui retourné par le service
        assertEquals(user, result);
    }
    @Test
    public void testLogout() {
        // Créer des objets simulés pour la requête et la réponse HTTP
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);

        // Capture de l'argument captor pour la session
        ArgumentCaptor<HttpSession> sessionCaptor = ArgumentCaptor.forClass(HttpSession.class);

        // Définir le comportement du mock de la requête pour retourner la session capturée
        when(request.getSession()).thenReturn(session);

        // Appeler la méthode du contrôleur à tester
        ResponseEntity<String> result = appUserController.logout(request, response);

        // Vérifier que la session a été invalidée
        verify(request, times(1)).getSession();
        verify(session, times(1)).invalidate();

        // Vérifier que la réponse renvoyée est correcte
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals("Logout successful", result.getBody());
    }



    @Test
    public void testSaveUser() {
        // Créez un utilisateur fictif à sauvegarder
        AppUser userToSave = new AppUser(1L, "user1", "password1", "user1@example.com", null);

        // Définir le comportement du mock du service lorsqu'il est appelé
        when(accountService.addNewUser(userToSave)).thenReturn(userToSave);

        // Appeler la méthode du contrôleur à tester
        AppUser result = appUserController.saveUser(userToSave);

        // Vérifier si la méthode du service a été appelée une fois
        verify(accountService, times(1)).addNewUser(userToSave);

        // Vérifier si l'utilisateur retourné par le contrôleur correspond à celui retourné par le service
        assertEquals(userToSave, result);
    }

    // Ajoutez d'autres méthodes de test pour couvrir toutes les fonctionnalités du contrôleur
}
