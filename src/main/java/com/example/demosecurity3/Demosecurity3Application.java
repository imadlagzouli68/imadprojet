package com.example.demosecurity3;

import com.example.demosecurity3.config.entities.AppRole;
import com.example.demosecurity3.config.entities.AppUser;
import com.example.demosecurity3.config.service.AccountService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;

import java.util.ArrayList;

@SpringBootApplication
public class Demosecurity3Application {

    public static void main(String[] args) {
        SpringApplication.run(Demosecurity3Application.class, args);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Bean
    public CommandLineRunner commandLineRunnerUserDetails(AccountService accountService) {
        return args -> {
            // Vérifier si un utilisateur existe déjà avant de l'ajouter
            AppUser meryam = accountService.loadUserByUsername("meryam");
            if (meryam == null) {
                meryam = new AppUser(null, "meryam", "suchi", "mery@gmail.com", new ArrayList<>());
                accountService.addNewUser(meryam);
                accountService.addRoleToUser("meryam", "USER");
            }

            // Répétez ces étapes pour chaque utilisateur
            // Assurez-vous de vérifier l'existence de chaque utilisateur avant de l'ajouter

            // Exemple pour "hamza"
            AppUser hamza = accountService.loadUserByUsername("hamza");
            if (hamza == null) {
                hamza = new AppUser(null, "hamza", "camel", "hamza@gmail.com", new ArrayList<>());
                accountService.addNewUser(hamza);
                accountService.addRoleToUser("hamza", "USER");

            }
            else {
                System.out.println("L'utilisateur 'meryam' existe déjà.");
            }


            // Exemple pour "ayoub"
            AppUser ayoub = accountService.loadUserByUsername("ayoub");
            if (ayoub == null) {
                ayoub = new AppUser(null, "ayoub", "lear", "ayoub@gmail.com", new ArrayList<>());
                accountService.addNewUser(ayoub);
                accountService.addRoleToUser("ayoub", "USER");
                accountService.addRoleToUser("ayoub", "ADMIN");
            }
            else {
                System.out.println("L'utilisateur 'hamza' existe déjà.");
            }


            // Exemple pour "thami"
            AppUser thami = accountService.loadUserByUsername("thami");
            if (thami == null) {
                thami = new AppUser(null, "thami", "root", "thami@gmail.com", new ArrayList<>());
                accountService.addNewUser(thami);
                accountService.addRoleToUser("thami", "USER");
                accountService.addRoleToUser("thami", "ADMIN");
            }

            // Exemple pour "imad"
            AppUser imad = accountService.loadUserByUsername("imad");
            if (imad == null) {
                imad = new AppUser(null, "imad", "123", "imad@gmail.com", new ArrayList<>());
                accountService.addNewUser(imad);
                accountService.addRoleToUser("imad", "USER");
            }
        };
    }

}