package com.example.demosecurity3.config;


import com.example.demosecurity3.config.entities.AppUser;


import com.example.demosecurity3.config.filters.JwtAthenticationFilter;
import com.example.demosecurity3.config.filters.JwtAuthorizationFilter;
import com.example.demosecurity3.config.service.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

@Configuration
@EnableWebSecurity
@AllArgsConstructor
public class SecurityConfig extends  WebSecurityConfigurerAdapter  {
    private AccountService accountService;

    protected  void configure(AuthenticationManagerBuilder auth) throws  Exception{
        auth.userDetailsService(new UserDetailsService() {

            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                AppUser appUser = accountService.loadUserByUsername(username);
                if (appUser == null) {
                    throw new UsernameNotFoundException("Utilisateur non trouvé avec le nom d'utilisateur: " + username);
                }
                Collection<GrantedAuthority> authorities = new ArrayList<>();
                appUser.getAppRoles().forEach(r -> {
                    authorities.add(new SimpleGrantedAuthority(r.getRolename()));
                });
                return new User(appUser.getUsername(), appUser.getPassword(), authorities);
            }

        });
    }





@Override
protected void configure(HttpSecurity http) throws Exception {

    http

                .cors().and() // Activer la gestion des CORS
                .authorizeRequests()
                .antMatchers("/login").permitAll() // Autoriser l'accès à /login sans authentification
                .antMatchers("/refreshToken/**").permitAll() // Autoriser l'accès à /refreshToken/** sans authentification
                .antMatchers(HttpMethod.POST, "/users/**").hasAuthority("ADMIN")
                 .antMatchers(HttpMethod.PUT, "/users/**").hasAuthority("ADMIN")
                 .antMatchers(HttpMethod.DELETE, "/users/**").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.GET, "/users/**").hasAuthority("USER")
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(new JwtAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
                .addFilter(new JwtAthenticationFilter(authenticationManagerBean()))
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    http.csrf().disable();
    http.formLogin();
             // Spécifier l'URL de traitement du formulaire de connexion
    }




    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("http://localhost:3000"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
        configuration.setAllowedHeaders(Arrays.asList("*"));
        configuration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
@Bean
@Override
    public AuthenticationManager authenticationManagerBean()throws  Exception{
        return super.authenticationManagerBean();
}



}
