package com.example.demosecurity3.config.service;


import com.example.demosecurity3.config.entities.AppRole;
import com.example.demosecurity3.config.entities.AppUser;
import com.example.demosecurity3.config.repository.AppRoleRepository;
import com.example.demosecurity3.config.repository.AppUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
@AllArgsConstructor
public class AccountServiceImpl implements AccountService {
    private AppUserRepository appUserRepository;
    private AppRoleRepository appRoleRepository;
    private PasswordEncoder passwordEncoder;
    @Override
    public AppUser addNewUser(AppUser appUser) {
        String pw=appUser.getPassword();
        appUser.setPassword(passwordEncoder.encode(pw));

        return appUserRepository.save(appUser);
    }

    @Override
    public AppRole addNewRole(AppRole appRole) {


        return appRoleRepository.save(appRole);
    }

    @Override
    public void addRoleToUser(String username, String rolename) {
        AppUser appUser = appUserRepository.findByUsername(username);
        AppRole appRole = appRoleRepository.findByRolename(rolename);

        // Assurez-vous que la liste des rôles de l'utilisateur n'est pas null
        if (appUser.getAppRoles() == null) {
            appUser.setAppRoles(new ArrayList<>());
        }

        // Ajoutez le rôle à la liste des rôles de l'utilisateur
        appUser.getAppRoles().add(appRole);

        // Sauvegardez l'utilisateur mis à jour
        appUserRepository.save(appUser);
    }






    @Override
    public void removeRoleFromUser(String username, String rolename) {
        AppUser appUser=appUserRepository.findByUsername(username);
        AppRole appRole=appRoleRepository.findByRolename(rolename);
        appUser.getAppRoles().remove(appRole);
    }

    @Override
    public AppUser loadUserByUsername(String username) {
        return appUserRepository.findByUsername(username);
    }

    @Override
    public List<AppUser> listUsers() {
        return appUserRepository.findAll();
    }
}