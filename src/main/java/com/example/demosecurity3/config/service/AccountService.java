package com.example.demosecurity3.config.service;

import com.auth0.jwt.JWT;

import com.auth0.jwt.algorithms.Algorithm;

import com.example.demosecurity3.config.entities.AppRole;
import com.example.demosecurity3.config.entities.AppUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;

import java.util.Date;
import java.util.List;

public interface AccountService {
    AppUser addNewUser(AppUser appUser);
    AppRole addNewRole(AppRole appRole);
    void  addRoleToUser(String username , String rolename);

    void removeRoleFromUser(String username,String rolename);
    AppUser loadUserByUsername(String username);
    List<AppUser>listUsers();

    public default String generateAccessToken(Authentication authentication) {
        // Récupérez les informations de l'utilisateur à partir de l'objet Authentication
        User user=(User) authentication.getPrincipal();
        // Vous pouvez également obtenir d'autres informations de l'utilisateur si nécessaire

        // Générez le jeton JWT avec les informations de l'utilisateur
        Algorithm algorithm = Algorithm.HMAC256("mySecret1234");

        String accessToken = JWT.create()
                .withSubject(user.getUsername()) // Sujet du jeton (généralement le nom d'utilisateur)
                .withExpiresAt(new Date(System.currentTimeMillis() + 24 * 60 * 60 * 1000)) // Date d'expiration du jeton (24 heures dans cet exemple)
                // Vous pouvez également ajouter d'autres revendications au jeton si nécessaire
                .sign(algorithm); // Signez le jeton avec l'algorithme spécifié

        return accessToken;
    }


}
