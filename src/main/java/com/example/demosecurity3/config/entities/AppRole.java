package com.example.demosecurity3.config.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class AppRole {
    @Id@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String rolename;
}
