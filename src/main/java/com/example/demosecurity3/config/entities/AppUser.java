package com.example.demosecurity3.config.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class AppUser {
    @Id
    @GeneratedValue(strategy =GenerationType.IDENTITY)
    private  Long id;
    @Column(unique = true)
    private String username;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private  String password;
    private String email;
    @ManyToMany(fetch= FetchType.EAGER)
    private Collection<AppRole> appRoles=new ArrayList<>();


}
