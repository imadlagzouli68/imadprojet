package com.example.demosecurity3.config.repository;

import com.example.demosecurity3.config.entities.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppRoleRepository extends JpaRepository<AppRole, Long> {
    AppRole findByRolename(String roleName);
}
